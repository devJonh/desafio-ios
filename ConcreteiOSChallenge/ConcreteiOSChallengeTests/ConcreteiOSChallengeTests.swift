//
//  ConcreteiOSChallengeTests.swift
//  ConcreteiOSChallengeTests
//
//  Created by Jonatha Lima on 27/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import XCTest
@testable import ConcreteiOSChallenge

class ConcreteiOSChallengeTests: XCTestCase {
    
    func testLocalized() {
        
        let string = "xc_tests".localized
        XCTAssertEqual(string, "XCTests")
        
    }
    
}
