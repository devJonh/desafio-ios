//
//  MainViewControllerTests.swift
//  ConcreteiOSChallengeTests
//
//  Created by Jonatha Lima on 27/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import XCTest

class MainViewControllerTests: XCTestCase {
    
    let mainSB = UIStoryboard(name: "Main", bundle: nil)
    
    func testViewControllers() {
        
        let mainViewController = mainSB.instantiateViewController(withIdentifier: "MainViewController")
        XCTAssertNotNil(mainViewController)
        
    }
    
}
