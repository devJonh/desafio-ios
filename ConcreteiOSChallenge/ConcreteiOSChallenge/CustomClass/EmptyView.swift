//
//  EmptyView.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 26/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import UIKit

class EmptyView: UIView {
 
    @IBOutlet weak var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }
    
}

// MARK: - Private Methods
private extension EmptyView {
    
    func initSubviews() {
        Bundle.main.loadNibNamed("EmptyView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        super.backgroundColor = UIColor.clear
    }
    
}
