//
//  MainViewController.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 25/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    fileprivate var pageNumber = 1
    fileprivate let identifier = "RepositoryCell"
    
    fileprivate var repositories = [Repository]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initSubviews()
        self.title = "github_title".localized
        
        self.addLoadingActivity(from: tableView)
        self.listRepositories(by: .java, isScrolling: false)
        
    }
    
}

// MARK: - Private methods
private extension MainViewController {
    
    func initSubviews() {
        
        let repositoyCellNib = UINib(nibName: "RepositoryCell", bundle: nil)
        tableView.register(repositoyCellNib, forCellReuseIdentifier: identifier)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell") as? RepositoryCell {
            
            let repository = repositories[indexPath.row]
            cell.nameLabel.text = repository.name
            cell.descriptionLabel.text = repository.desc
            cell.forkAmountLabel.text = String(repository.forkAmount)
            cell.starAmountLabel.text = String(repository.starAmount)
            cell.usernameLabel.text = repository.owner.username
            cell.userImage.downloadImage(from: repository.owner.profileImage)
            return cell
            
        }
        
        return UITableViewCell()
        
    }
    
}

// MARK: - UITableViewDelegate
extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        if let pullRequestVC = mainSB.instantiateViewController(withIdentifier: "PullRequestViewController") as? PullRequestViewController {
            
            pullRequestVC.repository = repositories[indexPath.row]
            self.navigationController?.pushViewController(pullRequestVC, animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.repositories.count - 1 {
            
            self.pageNumber += 1
            
            listRepositories(by: .java, isScrolling: true)
            
        }
        
    }
    
}

// MARK: - Services Call
extension MainViewController {
    
    func listRepositories(by language: ProgrammingLanguage, isScrolling: Bool) {
        
        GitHubService().getAllRepositories(by: language, to: self.pageNumber, total: self.itemsPerPage) { success, repositories in
            
            if !success {
                return
            }
            
            if let repositoriesResult = repositories {
                
                if !isScrolling {
                    
                    self.repositories = repositoriesResult
                    
                } else if repositoriesResult.count != 0 {
                    
                    self.repositories += repositoriesResult
                    
                } else {
                    
                    self.tableView.tableFooterView = UIView()
                    
                }
                
            }
            
        }
        
    }
    
}
