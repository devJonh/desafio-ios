//
//  PullRequestViewController.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 26/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import UIKit
import SafariServices

class PullRequestViewController: BaseViewController {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var openedAmountLabel: UILabel!
    @IBOutlet weak var closedAmountLabel: UILabel!
    
    var repository: Repository?
    fileprivate var pageNumber = 1
    fileprivate var openedAmount = 0
    fileprivate var closedAmount = 0
    
    private let emptyView = EmptyView()
    
    fileprivate var pullRequests = [PullRequest]() {
        didSet {
            self.populate(from: pullRequests)
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initSubviews()
        
        if let repo = repository {
            self.addLoadingActivity(from: tableView)
            self.getPullRequests(by: repo.owner, and: repo, isScrolling: false)
        }
        
    }
    
}

// MARK: - Private Methods
private extension PullRequestViewController {
    
    func initSubviews() {
        
        self.title = repository != nil ? repository!.name : ""
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        
        self.backView.addShadow()
        
    }
    
    func populate(from pullRequests: [PullRequest]) {
        
        self.openedAmount = 0
        self.closedAmount = 0
        
        for pullRequest in pullRequests {
            
            if let state = PullRequestState(rawValue: pullRequest.state!) {
                
                switch state {
                case .all:
                    break
                case .open:
                    self.openedAmount += 1
                case .closed:
                    self.closedAmount += 1
                }
                
            }
            
        }
        
        self.openedAmountLabel.text = (openedAmount <= 1 ? "opened_number".localized : "opened_plural_number".localized)
            .replacingOccurrences(of: "#value#", with: String(openedAmount))
        
        self.closedAmountLabel.text = (closedAmount <= 1 ? "closed_number".localized : "closed_plural_number".localized)
            .replacingOccurrences(of: "#value#", with: String(closedAmount))
        
    }
    
}

// MARK: - UITableViewDataSource
extension PullRequestViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tableView.backgroundView = pullRequests.count != 0 ? UIView() : emptyView
        return pullRequests.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestCell") as? PullRequestCell {
            
            let pullRequest = pullRequests[indexPath.row]
            cell.titleLabel.text = pullRequest.title
            cell.descriptionLabel.text = pullRequest.desc
            cell.usernameLabel.text = pullRequest.owner.username
            cell.userImageView.downloadImage(from: pullRequest.owner.profileImage)
            return cell
            
        }
        
        return UITableViewCell()
        
    }
    
}

// MARK: - UITableViewDelegate
extension PullRequestViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let pullRequestLink = pullRequests[indexPath.row].url,
            let pullRequestURL = URL(string: pullRequestLink) else {
            return
        }

        if #available(iOS 9.0, *) {

            let safariVC = SFSafariViewController(url: pullRequestURL)
            safariVC.delegate = self
            self.present(safariVC, animated: true, completion: nil)

            startLoading()
            UIApplication.shared.statusBarStyle = .default

        } else {
            
            if let url = URL(string: pullRequestLink) {
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:])
                } else {
                    UIApplication.shared.openURL(pullRequestURL)
                }
                
            }
            
        }
    
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.pullRequests.count - 1 {
            
            self.pageNumber += 1
            
            if let repo = repository {
            
                self.getPullRequests(by: repo.owner, and: repo, isScrolling: true)
                
            }
            
        }
        
    }
    
}

// MARK: - SFSafariViewControllerDelegate
@available(iOS 9.0, *)
extension PullRequestViewController: SFSafariViewControllerDelegate {
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        stopLoading()
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
}

// MARK: - Services Call
extension PullRequestViewController {
    
    func getPullRequests(by user: User, and repository: Repository, isScrolling: Bool) {
        
        GitHubService().getPullRequests(from: user, and: repository, to: self.pageNumber, total: self.itemsPerPage) { (success, pullRequests) in
            
            if !success {
                return
            }
            
            if let pullRequestsReturned = pullRequests {
                
                if !isScrolling && pullRequestsReturned.count != 0 {
                    
                    self.pullRequests = pullRequestsReturned
                    
                } else if pullRequestsReturned.count != 0 {
                
                    self.pullRequests += pullRequestsReturned
                    
                } else {
                    
                    self.tableView.tableFooterView = UIView()
                    
                }
                
            }
            
        }
        
    }
    
}

// MARK: - Cell Class
class PullRequestCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userImageView.layer.cornerRadius = 10
        self.userImageView.layer.masksToBounds = true
    }
    
}
