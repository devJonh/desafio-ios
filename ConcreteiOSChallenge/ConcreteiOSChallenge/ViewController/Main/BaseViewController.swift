//
//  BaseViewController.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 25/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController {

    open let itemsPerPage = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSimpleBackBarButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}

// MARK: - Private Methods
private extension BaseViewController {
    
    func addSimpleBackBarButton() {
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
}

// MARK: - NVActivityIndicatorView
extension BaseViewController: NVActivityIndicatorViewable {
    
    func startLoading(with text:String? = "") {
        
        let bestSize = CGSize(width: 50, height: 50)
        let bestFont = UIFont.boldSystemFont(ofSize: 14)
        let backgroundColor = UIColor(white: 0.2, alpha: 0.6)
        
        self.startAnimating(bestSize, message: text, messageFont: bestFont, type: .ballSpinFadeLoader, color: .white, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 2, backgroundColor: backgroundColor, textColor: .white)
        
    }
    
    func stopLoading() {
        
        self.stopAnimating()
        
    }

    func addLoadingActivity(from tableView: UITableView) {
        
        let bestPadding = CGFloat(5)
        let bestSize = CGRect(x: 0, y: 0, width: 50, height: 50)
        let loadingActivity = NVActivityIndicatorView(frame: bestSize, type: .ballClipRotate, color: .appLightBlue, padding: bestPadding)
        let loadingActivityView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 70))
        
        loadingActivity.startAnimating()
        
        loadingActivityView.addSubview(loadingActivity)
        loadingActivity.center = loadingActivityView.center
        
        tableView.tableFooterView = loadingActivityView
        
    }
    
}
