//
//  TypeAliases.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 25/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import Foundation

typealias GitHubPullRequestCompletion = (_ success:Bool, _ pullRequests:[PullRequest]?)->()
typealias GitHubSearchRepositoriesCompletion = (_ success:Bool, _ repositories:[Repository]?)->()

