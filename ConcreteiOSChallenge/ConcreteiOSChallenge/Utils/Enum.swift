//
//  Enum.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 25/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import Foundation

enum ProgrammingLanguage: String {
    
    case java
    case swift
    case cSharp = "c#"
    
}

enum PullRequestState: String {
    
    case open
    case closed
    case all
    
}
