//
//  Extensions.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 25/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import UIKit
import SDWebImage

// MARK: -
extension UIColor {
    
    static var appDarkBlue: UIColor = {
        return UIColor.fromRGB("343438")
    }()
    
    static var appLightBlue: UIColor = {
        return UIColor.fromRGB("5A8EB6")
    }()
    
    static var appOrange: UIColor = {
        return UIColor.fromRGB("DD8F0F")
    }()
    
    static func fromRGB(_ colorCode: String, alpha: Float = 1.0) -> UIColor {
        let scanner = Scanner(string:colorCode)
        var color:UInt32 = 0;
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = CGFloat(Float(Int(color >> 16) & mask)  / 255.0)
        let g = CGFloat(Float(Int(color >> 8)  & mask)  / 255.0)
        let b = CGFloat(Float(Int(color )      & mask)  / 255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: CGFloat(alpha))
    }
    
}

// MARK: -
extension String {
    
    var localized: String {
        get {
            return NSLocalizedString(self, comment: "")
        }
    }
    
}

// MARK: -
extension UIImageView {
    
    func tintImage(with color: UIColor) {
        
        if let image = self.image {
            self.image = image.withRenderingMode(.alwaysTemplate)
            self.tintColor = color
        }
        
    }
    
    func downloadImage(from link:String) {
     
        if let imageURL = URL(string: link), let placeHolder = UIImage(named: "ph_person") {
            self.sd_setImage(with: imageURL, placeholderImage: placeHolder, options: .scaleDownLargeImages, completed: nil)
        }
        
    }
    
}

// MARK: -
extension UIView {
    
    func addShadow() {
        self.layer.shadowOffset = CGSize(width: -1, height: 2)
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.08
    }
    
}
