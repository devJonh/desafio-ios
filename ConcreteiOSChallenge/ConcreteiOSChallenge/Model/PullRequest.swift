//
//  PullRequest.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 26/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import ObjectMapper

class PullRequest: Mappable {
    
    var url: String?
    var title: String? = ""
    var desc: String? = ""
    var state: String?
    var owner = User()
    
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        
        url <- map["html_url"]
        title <- map["title"]
        desc <- map["body"]
        owner <- map["user"]
        state <- map["state"]
        
    }
    
}
