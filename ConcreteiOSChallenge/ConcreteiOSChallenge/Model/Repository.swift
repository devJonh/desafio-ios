//
//  Repository.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 25/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import UIKit
import ObjectMapper

class Repository: Mappable {
    
    var name: String = ""
    var desc: String = ""
    var forkAmount: Int = 0
    var starAmount: Int = 0
    var owner: User = User()
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name <- map["name"]
        desc <- map["description"]
        owner <- map["owner"]
        forkAmount <- map["forks_count"]
        starAmount <- map["stargazers_count"]
    }
    
}
