//
//  User.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 25/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import UIKit
import ObjectMapper

class User: Mappable {

    var username: String = ""
    var profileImage: String = ""
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        username <- map["login"]
        profileImage <- map["avatar_url"]
    }
    
}
