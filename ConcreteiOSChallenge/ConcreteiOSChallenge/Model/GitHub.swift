//
//  GitHub.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 25/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import ObjectMapper

class GitHub: Mappable {

    var repositories = [Repository]()
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        repositories <- map["items"]
    }
    
}
