//
//  GitHubService.swift
//  ConcreteiOSChallenge
//
//  Created by Jonatha Lima on 25/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import Alamofire
import ObjectMapper

class GitHubService: BaseService {
    
    func getAllRepositories(by language: ProgrammingLanguage, to pageNumber: Int, total perPage: Int, completion: @escaping GitHubSearchRepositoriesCompletion) {
        
        let url = baseURL.appending("search/repositories?q=:\(language)&sort=stars&page=\(pageNumber)&per_page=\(perPage)")
        
        Alamofire.request(url).authenticate(user: "jonathalimax", password: "").responseJSON { (response) in
            
            if let data = response.data, let JSON = String(data: data, encoding: .utf8) {
                
                if let gitHub = Mapper<GitHub>().map(JSONString: JSON) {
                    
                    completion(response.result.isSuccess, gitHub.repositories)
                    
                }
                
            }
            
            completion(response.result.isSuccess, nil)
            
        }
        
    }
    
    func getPullRequests(from user: User, and repository: Repository, to pageNumber: Int, total perPage: Int, completion: @escaping GitHubPullRequestCompletion) {
        
        let url = baseURL.appending("repos/\(user.username)/\(repository.name)/pulls?page=\(pageNumber)&per_page=\(perPage)")
        
        Alamofire.request(url).authenticate(user: "jonathalimax", password: "").responseJSON { (response) in
            
            if let data = response.data, let JSON = String(data: data, encoding: .utf8) {
                
                if let pullRequests = Mapper<PullRequest>().mapArray(JSONString: JSON) {
                    
                    completion(response.result.isSuccess, pullRequests)
                    
                }
                
            }
            
            completion(response.result.isSuccess, nil)
            
        }
        
    }
    
}
