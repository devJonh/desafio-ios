//
//  ConcreteiOSChallengeUITests.swift
//  ConcreteiOSChallengeUITests
//
//  Created by Jonatha Lima on 27/01/18.
//  Copyright © 2018 jlima. All rights reserved.
//

import XCTest

class ConcreteiOSChallengeUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        XCUIApplication().launch()
    }
    
    func testExample() {
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM."]/*[[".cells.staticTexts[\"RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM.\"]",".staticTexts[\"RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM.\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["2.x: Improve the wording of the share() JavaDocs"]/*[[".cells.staticTexts[\"2.x: Improve the wording of the share() JavaDocs\"]",".staticTexts[\"2.x: Improve the wording of the share() JavaDocs\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
    }
    
}
